# Incremental

> Page view counter and alerts prioritizing performance and privacy.

daily resolution

- track impressions
- track views (uniques, rolling hourly)
- origin for validation
- alert for significant change in traffic
  - identify referrer triggering spike
- only log referrers for purpose of identifying source of spike

## Lambda aliases

## API Gateway

1. Create resource
2. Create method

```
incremental_incr:${stageVariables.alias}
```

Lambda functions must exist in each stage before the permissions can be set for the aliases

```
aws lambda add-permission --function-name arn:aws:lambda:us-west-2:107305737797:function:incremental_incr:prod --source-arn 'arn:aws:execute-api:us-west-2:107305737797:tf3w1a8ql5/*/GET/incr' --principal apigateway.amazonaws.com --statement-id 250801ee-e8d6-4320-88d2-d7f2011a3783 --action lambda:InvokeFunction --profile jamesr
```

## DynamoDB

1. Create (switch to uppercase table names)
2. Permissions
