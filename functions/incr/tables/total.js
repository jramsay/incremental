const { db, dynamo } = require('../db');
const { msToSeconds } = require('../util.js');

const TABLE_NAME = 'Incremental_Total';
const TABLE_SCHEMA = {
  AttributeDefinitions: [
    {
      AttributeName: 'Url',
      AttributeType: 'S',
    },
  ],
  KeySchema: [
    {
      AttributeName: 'Url',
      KeyType: 'HASH',
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5,
  },
  TableName: TABLE_NAME,
};

// Event totals
// - impression: increment by [0, 1]
// - unique: increment by [0, 1]
// - IDEA: claps (e.g. Medium)
// - IDEA: emjoi reactions (e.g. Slack emoji reactions)
function increment(url, events, cb) {
  const now = msToSeconds(Date.now());
  const params = {
    TableName: TABLE_NAME,
    Key: { Url: url },
    UpdateExpression:
      'ADD CountImpression :impression, CountUnique :unique SET LastUpdated = :updatedAt',
    ExpressionAttributeValues: {
      ':impression': events.impression,
      ':unique': events.unique,
      ':updatedAt': now,
    },
    ReturnValues: 'ALL_NEW',
  };
  return db.update(params, (ex, record) => {
    if (ex) return cb(ex);
    const results = {
      impression: record.Attributes.CountImpression,
      unique: record.Attributes.CountUnique,
    };
    return cb(null, results);
  });
}

// INTERNAL
function write(url, events, cb) {
  const now = msToSeconds(Date.now());
  const params = {
    TableName: TABLE_NAME,
    Key: { Url: url },
    UpdateExpression:
      'ADD CountImpression :impression, CountUnique :unique SET LastUpdated = :updatedAt',
    ExpressionAttributeValues: {
      ':impression': events.impression,
      ':unique': events.unique,
      ':updatedAt': now,
    },
    ReturnValues: 'ALL_NEW',
  };
  return db.update(params, cb);
}

module.exports = {
  TABLE_NAME,
  TABLE_SCHEMA,
  increment,
  write,
};
