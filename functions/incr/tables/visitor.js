const { dynamo, db } = require('../db');
const { msToSeconds } = require('../util');

const TABLE_NAME = 'Incremental_Visitors';
const TABLE_SCHEMA = {
  AttributeDefinitions: [
    {
      AttributeName: 'Visitor',
      AttributeType: 'S',
    },
  ],
  KeySchema: [
    {
      AttributeName: 'Visitor',
      KeyType: 'HASH',
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 5,
    WriteCapacityUnits: 5,
  },
  TableName: TABLE_NAME,
};
const EXPIRE_TTL = 3600; // 1hr in seconds

function visit(visitor, cb) {
  const now = msToSeconds(Date.now());
  const params = {
    TableName: TABLE_NAME,
    Key: { Visitor: visitor },
    UpdateExpression: 'SET Expire = :expire',
    ConditionExpression: 'attribute_not_exists(Expire) OR Expire < :now',
    ExpressionAttributeValues: {
      ':now': now,
      ':expire': now + EXPIRE_TTL,
    },
    ReturnValues: 'UPDATED_NEW',
  };

  db.update(params, (err, result) => {
    if (err && err.code !== 'ConditionalCheckFailedException') return cb(err);
    const isView = result !== null;
    return cb(null, isView);
  });
}

// Internal use only
function write(visitor, { expire }, cb) {
  const params = {
    TableName: TABLE_NAME,
    Key: { Visitor: visitor },
    UpdateExpression: 'SET Expire = :expire',
    ExpressionAttributeValues: {
      ':expire': expire,
    },
    ReturnValues: 'ALL_NEW',
  };
  return db.update(params, cb);
}

module.exports = {
  TABLE_NAME,
  TABLE_SCHEMA,
  visit,
  write,
};
