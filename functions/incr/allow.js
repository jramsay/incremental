const memoize = require('fast-memoize');

const localhost = /https?:\/\/localhost:\d+/;
const whitelist = [/(.*.)?jramsay.com.au/, /(.*.)?example.com/];

function allow(origin) {
  const match = whitelist.find(pattern => pattern.test(origin));
  const headers = {
    'Access-Control-Allow-Origin': origin,
    'Access-Control-Allow-Methods': 'OPTIONS, GET',
    Vary: 'Origin',
  };

  if (match) return headers;

  // Allow localhost usage, but communicate only random data will be returned
  if (localhost.test(origin)) {
    headers['X-Development-Mode'] = 'random';
    return headers;
  }

  return null;
}

module.exports = memoize(allow);
