const requestIp = require('request-ip');
const normalizeUrl = require('normalize-url');
const lowercaseKeys = require('lowercase-keys');
const { hash } = require('./util');
const { visit } = require('./tables/visitor.js');
const { increment } = require('./tables/total.js');
const allow = require('./allow.js');

// Only permit clean annonymized data into the main program loop
function clean(req) {
  const { httpMethod } = req;

  // Inconsistent use of `Origin` and `origin`
  const headers = lowercaseKeys(req.headers);

  const visitorIp = requestIp.getClientIp({
    headers,
  });

  // HTTP uses referer, DOM uses referrer
  const { origin, referer } = headers;

  // TODO: validation for null referer
  const url = normalizeUrl(hash(referer));
  const visitor = hash(url, visitorIp);

  return { origin, url, visitor, httpMethod };
}

function main({ origin, url, visitor, httpMethod }, context, cb) {
  const headers = allow(origin);

  if (headers === null) return cb('Forbidden Origin');

  const res = {
    statusCode: 200,
    headers,
  };

  if (httpMethod === 'OPTIONS') return cb(null, res);

  // Localhost: skip all db calls and return random data
  if (headers['X-Development-Mode'] === 'random') {
    const MAX = 20000;
    const MIN = 50;
    const impression = Math.floor(Math.random() * (MAX - MIN)) + MIN;
    const unique = Math.floor(Math.random() * (impression - MIN)) + MIN;
    res.body = JSON.stringify({
      impression,
      unique,
      events: {
        impression: 1,
        unique: 1,
      },
    });
    return cb(null, res);
  }

  return visit(visitor, (err, isUnique) => {
    if (err) return cb(err);

    const events = {
      impression: 1,
      unique: isUnique ? 1 : 0,
    };

    return increment(url, events, (ex, { impression, unique }) => {
      if (ex) return cb(ex);
      const body = {
        impression,
        unique,
        events,
      };
      res.body = JSON.stringify(body);
      return cb(null, res);
    });
  });
}

function handle(e, context, cb) {
  const sanitizedEvent = clean(e);
  return main(sanitizedEvent, context, cb);
}

module.exports = { handle };
