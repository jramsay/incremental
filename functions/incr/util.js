const crypto = require('crypto');

// Generate sha256 hash of string arguments
function hash(...items) {
  const strings = items.filter(i => typeof i === 'string');
  return crypto
    .createHash('sha256')
    .update(strings.join(''))
    .digest('hex');
}

// Convert milliseconds to seconds
// Bitwise `| 0` converts float to integer
function msToSeconds(ms) {
  // eslint-disable-next-line no-bitwise
  return (ms / 1000) | 0;
}

module.exports = {
  hash,
  msToSeconds,
};
