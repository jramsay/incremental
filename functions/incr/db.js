const AWS = require('aws-sdk');

const dynamoConfig = {};

if (process.env.NODE_ENV === 'test') {
  AWS.config.update({ region: 'us-east-1' });
  dynamoConfig.endpoint = 'http://localhost:4567';
}

const dynamo = new AWS.DynamoDB(dynamoConfig);
const db = new AWS.DynamoDB.DocumentClient(dynamoConfig);

module.exports = {
  dynamo,
  db,
};
