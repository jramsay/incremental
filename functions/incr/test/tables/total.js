const test = require('ava');
const db = require('../helpers/db.js');
const { hash } = require('../../util');
const total = require('../../tables/total.js');

test.before.cb(t => {
  db.init(err => {
    if (err) t.fail(err);
    t.end();
  });
});

test.cb('increment create a new record for new url', t => {
  const testUrl = hash('http://example.com/total/test/1');
  const testEvents = { impression: 1, unique: 1 };
  total.increment(testUrl, testEvents, (err, result) => {
    t.ifError(err);
    t.deepEqual(result, {
      impression: 1,
      unique: 1,
    });
    t.end();
  });
});

test.cb('increment updates record fields by specified amount', t => {
  const testUrl = hash('http://example.com/total/test/2');
  const testEvents = { impression: 0, unique: 1 };
  total.write(testUrl, { impression: 12327, unique: 12222 }, ex => {
    t.ifError(ex);
    total.increment(testUrl, testEvents, (err, result) => {
      t.ifError(err);
      t.deepEqual(result, {
        impression: 12327,
        unique: 12223,
      });
      t.end();
    });
  });
});
