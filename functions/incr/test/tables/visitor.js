const test = require('ava');
const async = require('async');
const db = require('../helpers/db.js');
const { hash, msToSeconds } = require('../../util');
const visitor = require('../../tables/visitor.js');

test.before.cb(t => {
  db.init(err => {
    if (err) t.fail(err);
    t.end();
  });
});

test.cb('visit creates a new record for new visitor', t => {
  const testVisitor = hash('visitor1', Date.now().toString());
  visitor.visit(testVisitor, (err, result) => {
    t.ifError(err);
    t.is(result, true);
    t.end();
  });
});

test.cb('visit returns false for repeat visits', t => {
  const testVisitor = hash('visitor2', Date.now().toString());
  const visits = [testVisitor, testVisitor];
  async.mapSeries(
    visits,
    (v, cb) => visitor.visit(v, cb),
    (err, results) => {
      t.ifError(err);
      t.is(results[0], true);
      t.is(results[1], false);
      t.end();
    }
  );
});

test.cb('visit returns true for repeat visit after expiry', t => {
  const testVisitor = hash('visitor3', Date.now().toString());
  const visits = [testVisitor, testVisitor];
  const expiredTtl = msToSeconds(Date.now()) - 1000;
  visitor.write(testVisitor, { expire: expiredTtl }, ex => {
    t.ifError(ex);
    async.mapSeries(
      visits,
      (v, cb) => visitor.visit(v, cb),
      (err, results) => {
        t.ifError(err);
        t.is(results[0], true);
        t.is(results[1], false);
        t.end();
      }
    );
  });
});
