const test = require('ava');
const allow = require('../allow.js');

test('allow returns CORS headers for allowed Origin', t => {
  const headers = allow('https://example.com');
  t.deepEqual(headers, {
    'Access-Control-Allow-Origin': 'https://example.com',
    'Access-Control-Allow-Methods': 'OPTIONS, GET',
    Vary: 'Origin',
  });
});

test('allow returns null for unknown Origin', t => {
  const headers = allow('https://foo.com');
  t.is(headers, null);
});

test('allow returns CORS headers with custom header for localhost', t => {
  const headers = allow('http://localhost:3000');
  t.deepEqual(headers, {
    'Access-Control-Allow-Origin': 'http://localhost:3000',
    'Access-Control-Allow-Methods': 'OPTIONS, GET',
    'X-Development-Mode': 'random',
    Vary: 'Origin',
  });
});
