const async = require('async');
const { dynamo } = require('../../db.js');
const tableTotal = require('../../tables/total.js');
const tableVisitor = require('../../tables/visitor.js');

const tables = [tableTotal, tableVisitor];

function initTable(t, cb) {
  dynamo.describeTable({ TableName: t.TABLE_NAME }, err => {
    // description means the table exists
    if (!err) return cb(null);
    return dynamo.createTable(t.TABLE_SCHEMA, ex => {
      if (ex) return cb(ex);
      const params = {
        $waiter: {
          delay: 1,
          maxAttempts: 3,
        },
        TableName: t.TABLE_NAME,
      };
      return dynamo.waitFor('tableExists', params, cb);
    });
  });
}

function init(cb) {
  async.each(tables, (t, done) => initTable(t, done), err => cb(err));
}

module.exports = { init };
