const test = require('ava');
const fs = require('fs');
const path = require('path');
const incr = require('../index.js');
const db = require('./helpers/db.js');

// Mock data
const event = JSON.parse(
  fs.readFileSync(path.join(__dirname, 'fixtures', 'event.json'))
);
const context = JSON.parse(
  fs.readFileSync(path.join(__dirname, 'fixtures', 'context.json'))
);

test.before.cb(t => {
  db.init(err => {
    if (err) t.fail(err);
    t.end();
  });
});

test.cb('incr handle event', t => {
  incr.handle(event, context, (err, result) => {
    t.ifError(err);
    const body = JSON.parse(result.body);
    t.deepEqual(body, {
      impression: 1,
      unique: 1,
      events: {
        impression: 1,
        unique: 1,
      },
    });
    // TODO: verify expected visitor record
    // TODO: verify expected total record
    return t.end();
  });
});
