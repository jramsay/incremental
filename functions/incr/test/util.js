const test = require('ava');
const { hash, msToSeconds } = require('../util.js');

test('hash returns sha256 hash of input string', t => {
  const result = hash('foo bar');
  t.is(
    result,
    'fbc1a9f858ea9e177916964bd88c3d37b91a1e84412765e29950777f265c4b75'
  );
});

test('hash returns sha256 hash of concatenated string args', t => {
  const result = hash('foo', 1, ' ', 'bar');
  t.is(
    result,
    'fbc1a9f858ea9e177916964bd88c3d37b91a1e84412765e29950777f265c4b75'
  );
});

test('msToSeconds returns integer seconds', t => {
  const seconds = msToSeconds(13250);
  t.is(seconds, 13);
});

test('msToSeconds returns integer seconds', t => {
  const seconds = msToSeconds(17899);
  t.is(seconds, 17);
});
